﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "../Enums/AbilitiesInputID.h"
#include "Abilities/GameplayAbility.h"
#include "Templates/SubclassOf.h"
#include "UObject/ObjectPtr.h"

#include "CharacterAbilityConfig.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FCharacterAbilityConfig
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly)
	EAbilitiesInputID InputID = EAbilitiesInputID::None;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UGameplayAbility> Ability;
	UPROPERTY(EditDefaultsOnly)
	TObjectPtr<class UInputAction> Action;
};
