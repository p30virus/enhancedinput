// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EnhancedInputTutGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ENHANCEDINPUTTUT_API AEnhancedInputTutGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
