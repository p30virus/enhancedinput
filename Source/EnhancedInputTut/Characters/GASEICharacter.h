﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "InputActionValue.h"
#include "Abilities/GameplayAbility.h"
#include "Structs/CharacterAbilityConfig.h"
#include "GASEICharacter.generated.h"

UCLASS()
class ENHANCEDINPUTTUT_API AGASEICharacter : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:	
	// Sets default values for this character's properties
	AGASEICharacter();

protected:
	UPROPERTY(EditDefaultsOnly)
	TObjectPtr<class UInputMappingContext> MappingContext;
	
	UPROPERTY(VisibleAnywhere, Category="Components")
	TObjectPtr<class USpringArmComponent> SpringArm;

	UPROPERTY(VisibleAnywhere, Category="Components")
	TObjectPtr<class UCameraComponent> Camera;

	UPROPERTY(VisibleAnywhere, Category="Components")
	TObjectPtr<class UAbilitySystemComponent> AbilitySystem;
	// Called when the game starts or when spawned

	UPROPERTY(EditDefaultsOnly, Category="Abilities")
	FCharacterAbilityConfig MovementAbility;
	
	UPROPERTY(EditDefaultsOnly, Category="Abilities")
	FCharacterAbilityConfig JumpAbility;
	FGameplayAbilitySpecHandle JumpAbilitySpecHandle;

	void SetupInputMapping();

	void GiveAbilitiesToCharacter();
	
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	virtual void PawnClientRestart() override;

	void MovementAction(const FInputActionValue& ActionValue);
	void StartJumpAction();
	void StopJumpAction();
	
};
