// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InputActionValue.h"
#include "EICharacter.generated.h"

UCLASS()
class ENHANCEDINPUTTUT_API AEICharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEICharacter();

protected:

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TObjectPtr<class UInputAction> MovementAction;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TObjectPtr<class UInputAction> JumpAction;

	UPROPERTY(EditDefaultsOnly)
	TObjectPtr<class UInputMappingContext> MappingContext;
	
	UPROPERTY(VisibleAnywhere, Category="Components")
	TObjectPtr<class USpringArmComponent> SpringArm;

	UPROPERTY(VisibleAnywhere, Category="Components")
	TObjectPtr<class UCameraComponent> Camera;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	UFUNCTION()
	void ScrollMovementAction(const FInputActionValue& Value);
	UFUNCTION()
	void BeginJumpAction();
	UFUNCTION()
	void EndJumpAction();

	virtual void PawnClientRestart() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
