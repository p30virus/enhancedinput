﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GASEICharacter.h"

#include "AbilitySystemComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetSystemLibrary.h"


// Sets default values
AGASEICharacter::AGASEICharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	if (!SpringArm)
	{
		return;
	}
	SpringArm->SetupAttachment(GetRootComponent(), NAME_None);
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	if (!Camera)
	{
		return;
	}
	Camera->SetupAttachment(SpringArm, NAME_None);
	AbilitySystem = CreateDefaultSubobject<UAbilitySystemComponent>("Ability System");
}

void AGASEICharacter::SetupInputMapping()
{
	if (InputComponent)
	{
		UEnhancedInputComponent *EnhancedIn = Cast<UEnhancedInputComponent>(InputComponent);
		if(EnhancedIn)
		{
			if(MovementAbility.Action)
			{
				EnhancedIn->BindAction(MovementAbility.Action, ETriggerEvent::Triggered, this, &AGASEICharacter::MovementAction);
			}
			if(JumpAbility.Action)
			{
				EnhancedIn->BindAction(JumpAbility.Action, ETriggerEvent::Started, this, &AGASEICharacter::StartJumpAction);
				EnhancedIn->BindAction(JumpAbility.Action, ETriggerEvent::Completed, this, &AGASEICharacter::StopJumpAction);
			}
		}
	}
}

void AGASEICharacter::GiveAbilitiesToCharacter()
{
	if (AbilitySystem)
	{
		if (JumpAbility.Ability)
		{
			const FGameplayAbilitySpec AbilitySpecData = FGameplayAbilitySpec(JumpAbility.Ability, 1, static_cast<int32>(JumpAbility.InputID));
			JumpAbilitySpecHandle = AbilitySystem->GiveAbility(AbilitySpecData);
		}
	}
	if (InputComponent)
	{
		SetupInputMapping();
	}
}

void AGASEICharacter::PawnClientRestart()
{
	Super::PawnClientRestart();
	if (APlayerController* PC = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PC->GetLocalPlayer()))
		{
			Subsystem->ClearAllMappings();
			Subsystem->AddMappingContext(MappingContext, 0);
		}
	}
}

void AGASEICharacter::MovementAction(const FInputActionValue& ActionValue)
{
	AddMovementInput(GetActorForwardVector(), ActionValue[0]);
}

void AGASEICharacter::StartJumpAction()
{
	if (AbilitySystem)
	{
		AbilitySystem->AbilityLocalInputPressed(static_cast<int32>(JumpAbility.InputID));
	}
}

void AGASEICharacter::StopJumpAction()
{
	if (AbilitySystem)
	{
		AbilitySystem->AbilityLocalInputReleased(static_cast<int32>(JumpAbility.InputID));
	}
}

// Called when the game starts or when spawned
void AGASEICharacter::BeginPlay()
{
	Super::BeginPlay();
	
	if (AbilitySystem)
	{
		AbilitySystem->InitAbilityActorInfo(this, this);
		GiveAbilitiesToCharacter();
	}
}

// Called every frame
void AGASEICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AGASEICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	SetupInputMapping();
}

UAbilitySystemComponent* AGASEICharacter::GetAbilitySystemComponent() const
{
	return AbilitySystem;
}

