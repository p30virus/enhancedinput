// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/EICharacter.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
AEICharacter::AEICharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;\
	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	if (!SpringArm)
	{
		return;
	}
	SpringArm->SetupAttachment(GetRootComponent(), NAME_None);
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	if (!Camera)
	{
		return;
	}
	Camera->SetupAttachment(SpringArm, NAME_None);
}

// Called when the game starts or when spawned
void AEICharacter::BeginPlay()
{
	Super::BeginPlay();
}

void AEICharacter::ScrollMovementAction(const FInputActionValue& Value)
{
	FString string = FString::Printf( TEXT( "%s : %f" ), TEXT(__FUNCSIG__), Value[0] );
	UKismetSystemLibrary::PrintString(GetWorld(),string, true, false, FLinearColor::Blue,0.f);
	AddMovementInput(GetActorForwardVector(),Value[0]);
}

void AEICharacter::BeginJumpAction()
{
	UKismetSystemLibrary::PrintString(GetWorld(),TEXT(__FUNCSIG__), true, false, FLinearColor::Red,3.f);
	Jump();
}

void AEICharacter::EndJumpAction()
{
	UKismetSystemLibrary::PrintString(GetWorld(),TEXT(__FUNCSIG__), true, false, FLinearColor::Green,3.f);
	StopJumping();
}

void AEICharacter::PawnClientRestart()
{
	Super::PawnClientRestart();
	if (APlayerController* PC = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PC->GetLocalPlayer()))
		{
			Subsystem->ClearAllMappings();
			Subsystem->AddMappingContext(MappingContext, 0);
		}
	}
}

// Called every frame
void AEICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AEICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	UKismetSystemLibrary::PrintString(GetWorld(),TEXT(__FUNCTION__),true, false, FLinearColor::Gray, 5.f);
	if (UEnhancedInputComponent* PlayerEnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		if (MovementAction)
		{
			PlayerEnhancedInputComponent->BindAction(MovementAction, ETriggerEvent::Triggered, this, &AEICharacter::ScrollMovementAction);
		}
		if (JumpAction)
		{
			PlayerEnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &AEICharacter::BeginJumpAction);
			PlayerEnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &AEICharacter::EndJumpAction);
		}
		
	}	
}

